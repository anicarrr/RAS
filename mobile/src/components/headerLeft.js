import React from "react";
import {
    StyleSheet,
    View,
    Image,
    Platform
} from 'react-native';
import { _Header } from 'src/utils/images';

const HeaderLeft = () => {
    return (
    <View style={styles.container}>
        <Image
            style={[styles.size]}
            source={_Header.logoIcon}
        />
    </View>
)};

const styles = StyleSheet.create({
    container: {
        marginLeft: 15,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    size: {
        width: 65, 
        height: 42,
        flex: 0.9
    }
});

export default HeaderLeft;
