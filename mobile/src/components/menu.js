import React from "react";
import {
  StyleSheet,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { MenuItems } from 'src/data/menuDefinition';
import { List, ListItem, Avatar } from 'react-native-elements';
import Colors from 'src/utils/colors';

const _avatar = (_icon) => (
  <Avatar
    large
    rounded
    icon={_icon}
    overlayContainerStyle={styles.avatar}
  />
)

const getActionType = (type) => {
  const actions = {
    modal: (params, props) => props.onActiveModal(params),
    navigation: (params, props) => props.onNavigation(params.screenName),
    url: (params, props) => props.onOpenUrl(params.url())
  }

  return actions[type] || (() => { });
};

const Menu = (props) => (
  <ScrollView>
    <List containerStyle={styles.list}>
      {
        MenuItems.map((item, i) => {
          const { type, params } = item.action;
          const executeAction = getActionType(type);
          return (
            <ListItem
              avatar={_avatar(item.icon)}
              key={i}
              title={item.title}
              chevronColor={Colors.greenRas}
              containerStyle={styles.listItem}
              titleStyle={styles.title}
              underlayColor={'#061038'}
              component={TouchableOpacity}
              activeOpacity={0.85}
              titleNumberOfLines={2}
              onPress={() => executeAction(params, props)}
            />
          )
        })
      }
    </List>
  </ScrollView>
);

const styles = StyleSheet.create({
  list: {
    marginTop: 0,
    marginBottom: -1,
    borderTopWidth: 0,
    borderBottomWidth: 0
  },
  listItem: {
    backgroundColor: Colors.blueRas
  },
  title: {
    color: 'white',
    fontSize: 18

  },
  avatar: {
    backgroundColor: '#737DA9'
  }
});

export default Menu;
