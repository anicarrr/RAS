import React, { Component } from 'react';
import { TouchableOpacity, 
         Text,
         StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import Info from 'src/components/info';
import Dni from 'src/components/dni';
import Colors from 'src/utils/colors';

export default class RasModal extends Component {
    state = {
        isModalVisible: false,
    }

    content = {
        info: () => <Info onHideModal={this.hideModal} />,
        dni: (props) => <Dni onHideModal={this.hideModal} 
                             navigation={props.navigation} 
                             headerTitle={props.headerTitle} 
                             screen={props.screen} />
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.contentToShow) return;
        this.setState({
            isModalVisible: true
        });
    }

    hideModal = () => {
        return new Promise(resolve => {
            this.setState({ isModalVisible: false }, () => resolve())
        })
    };

    shouldComponentUpdate(nextProps) {
        return nextProps.contentToShow;
    }

    render() {
        const { contentToShow, ...otherProps } = this.props;
        const { isModalVisible } = this.state;
        return (
                <Modal
                  isVisible={isModalVisible}
                  animationIn={'fadeIn'}
                  animationOut={'fadeOut'}
                  animationInTiming={100}
                  animationOutTiming={100}
                  useNativeDriver={true}
                  onBackdropPress={this.hideModal}
                  onBackButtonPress={this.hideModal}
                >
                    <TouchableOpacity style={styles.closeButtonContainer} onPress={this.hideModal}>
                        <Text style={styles.closeButton}>x</Text>
                    </TouchableOpacity>
                    {
                        contentToShow && this.content[contentToShow](otherProps) 
                    }
                </Modal>
        )
    }
}

const styles = StyleSheet.create({
    closeButton: {
        fontSize: 30,
        color: Colors.greenRas
    },
    closeButtonContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
});