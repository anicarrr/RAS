import React from "react";
import {
    StyleSheet,
    View } from 'react-native';
import { Akira  } from 'react-native-textinput-effects';
import Colors from 'src/utils/colors';
import { Button } from 'react-native-elements';

export default class Dni extends React.Component {
    state = {
        dni: ''
    }

    handleNavigation = (props) => {
        const { dni } = this.state;
        if (!dni.length) return;
        this.setState({ showSpinner: true }, () => {
            props.onHideModal().then(() => {
                props.navigation('Carnet', { title: props.headerTitle, screen: props.screen, dni: dni });
            });      
        });
    }
    
    render() {
        const { props } = this;
        return (
            <View style={styles.content}>
                <Akira 
                    label={'Ingrese su DNI'}
                    labelStyle={{ color: Colors.blueRas }}
                    borderColor={Colors.greenRas}
                    keyboardType={'numeric'}
                    autoFocus={true}
                    maxLength={8}
                    onChangeText={dni => this.setState({ dni })}
                    inputStyle={{fontSize: 35, color: Colors.greenRas}}
                    value={this.state.dni}
                    style={{height: 72, width: 300}}
                />
                <View>
                    <Button
                        onPress={() => this.handleNavigation(props)}
                        icon={{ name: 'search', type: 'octicons' }}
                        backgroundColor={Colors.blueRas}
                        buttonStyle={styles.button}
                        title='Buscar' />
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({ 
    content: {
        backgroundColor: 'white',
        borderColor: 'rgba(0, 0, 0, 0.1)',
        paddingVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        opacity: 0.9
    },
    button: {
        marginVertical: 15,
        borderRadius: 5,
        width: 200
    }
});