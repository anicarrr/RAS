import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Card } from 'react-native-elements';
import ButtonLlamar from 'src/components/buttonLlamar';

export default class UrgenciasScreen extends React.Component {
    static navigationOptions = {
        title: 'Urgencias'
    };
    
    render() {
        return (
            <Card title='Centro Nacional de Atención' titleStyle={styles.titleSize}>
                <Text style={styles.textCenter}>
                    Uso exclusivo beneficiarios RAS
                </Text>
                <Text style={styles.textCenter}>
                    (24x7)
                </Text>
               <ButtonLlamar color="red" />
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    textCenter: {
        textAlign: 'center'
    },
    titleSize: {
        fontSize: 18
    }
});
