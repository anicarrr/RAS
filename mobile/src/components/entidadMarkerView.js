import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const EntidadMarkerView = (props) => (
    <View style={styles.container}>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.phone}>{props.phone}</Text>
        <Text style={styles.address}>{props.address}</Text>
        <Text style={styles.address}>{props.city}</Text>
    </View>
);

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 4
    }
});

export default EntidadMarkerView;