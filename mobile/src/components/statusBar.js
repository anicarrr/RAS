import React from "react";
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import Colors from 'src/utils/colors';

const StatusBar = (props) => {
    const backgroundColor = props.error ? 'red' : Colors.greenRas;
    return (
        <View style={[styles.alertContainer, { backgroundColor }]}>
            <Text style={styles.alertText}>{props.children}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    alertContainer: {
        zIndex: 1000,
        padding: 8,
        width: '100%'
    },
    alertText: {
        color: 'white',
        textAlign: 'center',
        lineHeight: 17,
        fontSize: 15
    }
});

export default StatusBar;