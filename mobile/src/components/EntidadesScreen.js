import React from 'react';
import { View, StyleSheet, Image, Platform } from 'react-native';
import MapView from 'react-native-maps';
import EntidadMarkerView from 'src/components/entidadMarkerView';
import { _Map } from 'src/utils/images';
import Services from 'src/utils/services';
import StatusBar from 'src/components/statusBar';

export default class EntidadesScreen extends React.Component {
    static navigationOptions = {
        title: 'Entidades RAS'
    };

    state = {
        entidades: [],
        error: false,
        isLoading: true,
        alertMessage: 'Cargando los datos de las entidades...'
    }

    fixPositionMarker = {};

    componentDidMount() {
        Services.getEntidades().then( data => {
            this.setState({
                entidades: data,
                isLoading: false,
                alertMessage: ''
            });
        }).catch( () => {
            this.setState({ 
                error: true,
                isLoading: false,
                alertMessage: 'No se puedo recuperar los datos de las entidades. Inténtelo mas tarde'
            });
        });
    }

    componentWillUpdate = (nextProps, nextState) => {
        this.fixPositionMarker = { x: 0, y: Platform.OS === "android" ? 0 : -25 };
    }

    mapMarkerAndroid = (item, i) => {
        return (
            <MapView.Marker
                key={i}
                coordinate={item.coordinates}
                zIndex={1000}
                image={_Map.marker}
            >
                <MapView.Callout
                    style={styles.info}
                >
                    <EntidadMarkerView {...item} />
                </MapView.Callout>
                
            </MapView.Marker>
        );
    }

    mapMarkerIOS = (item, i) => {
        return (
            <MapView.Marker
                key={i}
                coordinate={item.coordinates}
                zIndex={1000}
            >
                <Image
                    source={_Map.marker}
                    style={styles.marker}
                /> 
                <MapView.Callout
                    style={styles.info}
                >
                    <EntidadMarkerView {...item} />
                </MapView.Callout>

            </MapView.Marker>
        )
    }
    
    render() {
        const { entidades, error, isLoading, alertMessage } = this.state;
        return (
            <View style={styles.container}>
                {
                    (isLoading || error)  &&
                    <StatusBar error={error} >{alertMessage}</StatusBar>
                }
                <MapView
                    style={styles.map}
                    initialRegion={{
                        latitude: -34.6131231,
                        longitude: -58.383724,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                {
                    entidades.map((item, i) => (
                        Platform.OS === 'android'
                            ? this.mapMarkerAndroid(item, i)
                            : this.mapMarkerIOS(item, i)
                    ))
                }
                </MapView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject, 
    },
    marker: {
        height: 50,
        width: 37
    },
    info: {
        flex: 1,
        width: 200
    }
});