import React from 'react';
import { Image, 
         View, 
         StyleSheet, 
         TouchableOpacity,
         ToastAndroid,
         Platform,
         CameraRoll,
         ActivityIndicator } from 'react-native';
import Services from 'src/utils/services';
import Colors from 'src/utils/colors';
import StatusBar from 'src/components/statusBar';
import { Icon } from 'react-native-elements';
import RNFetchBlob from 'react-native-fetch-blob';
import Toast, {DURATION} from 'react-native-easy-toast';
import randomString from './../utils/randomString';

export default class CarnetScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: params.title,
            headerRight: params.headerRightCustom
        }
    };

    state = {
        loading: true,
        loadError: false,
        showDownloadCompleteMessage: false
    }

    url = '';
    hash = randomString(10); 
    errorMessage = 'Hubo un error durante la operación, no pudo recuperarse el carnet. Inténtelo mas tarde';
    errorMessageOnDownload = 'Hubo un error intentando descargar el carnet, inténtelo mas tarde';
    downloadCompleteMessage = 'El carnet se ha descargado en su dispositivo';

    getUrl = {
        carnetAfiliados: (dni) => Services.getCarnetDeAfiliados(dni),
        carnetBeneficios: (dni) => Services.getCarnetDeBeneficios(dni)
    }

    dirs = RNFetchBlob.fs.dirs;

    onDownloadFile = () => {
        if (Platform.OS === 'ios') {
            CameraRoll.saveToCameraRoll(this.url)
            .then(result => {
                this.refs.downloadComplete.show(this.downloadCompleteMessage, 3500)
            })
            .catch(err => {
                this.refs.downloadComplete.show(this.errorMessageOnDownload, 3500)
            })
        } else {
            //ANDROID
            let config = {
                addAndroidDownloads: {
                    useDownloadManager: true, 
                    notification: true,
                    mime: 'image/jpeg'
                }
            };
    
            RNFetchBlob
                .config(config)
                .fetch('GET', this.url)
                .then(resp => {
                    ToastAndroid.show(this.downloadCompleteMessage, ToastAndroid.LONG)
                })
                .catch(err => {
                    ToastAndroid.show(this.errorMessageOnDownload, ToastAndroid.LONG)
                })
        }
    }

    headerRightCustom = () => 
    {
        const iconSize = Platform.OS === "ios" ? 27 : 30;
        return (
            <View style={styles.header}>
                <View style={styles.buttonsRightContainer}>
                    <TouchableOpacity onPress={() => this.onDownloadFile()}>
                        <Icon
                            name='download'
                            type='material-community'
                            color='black'
                            size={iconSize}
                        />
                    </TouchableOpacity>
                </View>
            </View>
    )};

    handleOnImgError = () => {
        this.setState({ loadError: true }, () => {
            this.props.navigation.setParams({ headerRightCustom: '' });
        });
    }

    handleOnImgLoadEnd = () => {
        this.setState({ loading: false }, () => {
            this.props.navigation.setParams({ headerRightCustom: this.headerRightCustom() });
        });
    }

    render() {
        const { screen, dni } = this.props.navigation.state.params;
        const { loading, loadError, showDownloadCompleteMessage } = this.state;
        this.url = this.getUrl[screen](dni) + '&randomString=' + this.hash;
 
        return (
            <View style={styles.mainContainer}>
                {
                    loadError 
                ?
                    <StatusBar error={true}>{this.errorMessage}</StatusBar>
                :
                    <View style={styles.container}>
                        <Toast 
                            ref='downloadComplete' 
                            style={{backgroundColor:'grey'}}
                            opacity={0.9}
                        />
                    
                        <ActivityIndicator
                            size="large"
                            color={Colors.blueRas}
                            animating={loading}
                        />

                        <Image
                            style={styles.imageContent}
                            resizeMode="contain"
                            source={{ uri: this.url }}
                            onLoadEnd={() => this.handleOnImgLoadEnd()}
                            onError={() => this.handleOnImgError()}
                        />
                    </View>
                }
            </View>
           
       )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        ...StyleSheet.absoluteFillObject
    },
    imageContent: {
        ...StyleSheet.absoluteFillObject
    },
    mainContainer: {
        flex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row'
    },
    buttonsRightContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginRight: 15
    }
});
