import React from "react";
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Share,
    Platform
} from 'react-native';
import { Icon } from 'react-native-elements';
import Services from 'src/utils/services';

const onShareContent = () => {
  const url = Services.getStoreUrls()[Platform.OS];

  Share.share({
    message: `Descarga la App de la Red Argentina de Salud. Disponible para IOS y Android: ${url}`,
    url: url,
    title: 'Nueva App de la Red Argentina de Salud'
  }, {
      // Android only:
      dialogTitle: 'Compartí la app!'
    })
};

const HeaderRight = (props) => {
  const iconSize = Platform.OS === "ios" ? 30 : 30;
  return (
    <View style={styles.header}>
      <View style={styles.buttonsRightContainer}>
        <TouchableOpacity onPress={() => onShareContent()}>
          <Icon
            name='share'
            type='entypo'
            color='white'
            size={iconSize}
            iconStyle={styles.spacer}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => props.onActiveModal({contentToShow: 'info'})}> 
          <Icon
            name='info'
            type='feather'
            color='white'
            size={iconSize}
          />
        </TouchableOpacity>
      </View>
    </View>
)};

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonsRightContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginRight: 15
  },
  spacer: {
    marginRight: 30 
  }
});

export default HeaderRight;
