import React from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Linking,
    Clipboard,
    ToastAndroid,
    Platform
} from 'react-native';
import { Icon } from 'react-native-elements';
import Colors from 'src/utils/colors';

const onSendEmail = (email) => {
    var url = `mailto:${email}?subject=Consulta`;

    Linking.canOpenURL(url).then(supported => {
        if (supported) {
            Linking.openURL(url);
        } else {
            Clipboard.setString(email);
            Platform.OS === 'android' 
                ? ToastAndroid.show('Email copiado al portapapeles', ToastAndroid.SHORT)
                : {}
        }
    })
}

const ContactInfo = (props) => {
    return (
        <View style={styles.contactInfoContent}>
            <Icon
                {...props.icon}
                size={35}
                color='white'
                containerStyle={{marginRight: 10}}
            />
            <View>
                <Text size={10} style={{ color: 'white' }}>
                    {props.title}
                </Text>
                <TouchableOpacity onPress={() => onSendEmail(props.content)}>
                    <Text style={styles.link}>
                        {props.content}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const Info = () => ( 
    <View style={styles.content}>
        <Text style={styles.mainTitle}>
            Red Argentina de Salud
        </Text>
        <View>
            <ContactInfo title='Consultas' 
                         icon={{ name: 'mail', type: 'feather'}} 
                         content="redargentinadesalud@rascomra.com.ar" />
            <ContactInfo title='Soporte' 
                         icon={{ name: 'tools', type: 'entypo'}} 
                         content="soporte@rascomra.com.ar" />
        </View>
    </View>
);

const styles = StyleSheet.create({
    content: {
        flexDirection: 'column',
        backgroundColor: Colors.blueRas,
        opacity: 0.90,
        padding: 10,
        borderColor: Colors.greenRas,
        borderWidth: 1.5,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contactInfoContent: {
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    mainTitle: {
        fontSize: 20,
        marginBottom: 15,
        fontWeight: 'bold',
        color: 'white'
    },
    link: {
        fontSize: 13,
        color: Colors.greenRas,
        textDecorationLine: 'underline'
    }
});

export default Info;