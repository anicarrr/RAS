import React from 'react';
import { Text, View, StyleSheet, Linking, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import Services from 'src/utils/services';

const handlePhoneCall = () => {
    const url = `tel:${PhoneNumber}`;
    Linking.openURL(url);
};

const PhoneNumber = Services.getTelefonos().emergencia;

class ButtonLlamar extends React.Component {
    
    state = {
        phoneNumber: ''
    }
    
    componentWillMount = () => {
      this.setState({phoneNumber: Services.getTelefonos().emergencia})
    }

    render() {
        const { phoneNumber } = this.state;
        const { spacer, color } = this.props;
        const marginVertical = spacer || 0;
        const numberToShow = phoneNumber.match(/.{1,4}/g).join("-");
        return (
            <View style={{marginVertical}}>
                <Button
                    onPress={handlePhoneCall}
                    icon={{ name: 'local-phone' }}
                    backgroundColor={color}
                    buttonStyle={styles.button}
                    title='Llamar' />
                <Text style={[styles.textCenter, styles.bigger]}>
                    {numberToShow}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textCenter: {
        textAlign: 'center'
    },
    button: {
        marginVertical: 15,
        borderRadius: 25
    },
    bigger: {
        fontSize: 20,
        fontWeight: "500"
    }
});

export default ButtonLlamar;
