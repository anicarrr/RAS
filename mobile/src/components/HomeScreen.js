import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Linking
} from 'react-native';
import HeaderRight from 'src/components/headerRight';
import HeaderLeft from 'src/components/headerLeft';
import Menu from 'src/components/menu';
import Modal from 'src/components/modal';
import Colors from 'src/utils/colors';

export default class HomeScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            headerStyle: {
                backgroundColor: Colors.greenRas,
                height: 55 
            }, 
            headerLeft: <HeaderLeft />,
            headerRight: <HeaderRight onActiveModal={params.handleActiveModal} />,
        }
    };
    
    state = {
        contentToShow: '',
        headerTitle: '',
        screen: ''
    }

    handleActiveModal = (params) => {
        this.setState({
            contentToShow: params.contentToShow,
            headerTitle: params.headerTitle,
            screen: params.screen
        });
    }

    handleOpenUrl = (url) => {
        Linking.openURL(url);
    }

    componentDidMount() {
        this.props.navigation.setParams({ handleActiveModal: this.handleActiveModal });
    }

    render() {
        const { contentToShow, headerTitle, screen } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Menu onActiveModal={this.handleActiveModal} 
                      onNavigation={navigate} 
                      onOpenUrl={this.handleOpenUrl} />
                <Modal contentToShow={contentToShow} 
                       headerTitle={headerTitle}
                       screen={screen} 
                       navigation={navigate} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#737DA9'
    }
});
