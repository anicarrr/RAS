import React from 'react';
import { Text, ScrollView, StyleSheet } from 'react-native';
import { Card } from 'react-native-elements';
import ButtonLlamar from 'src/components/buttonLlamar';
import Services from 'src/utils/services';

export default class FaqScreen extends React.Component {
    static navigationOptions = {
        title: 'Preguntas Frecuentes'
    };

    state = {
        data: []
    }

    componentWillMount = () => {
        var faqData = Services.getFaq();
        var data = faqData.map(item => {
            const { content, ...rest } = item;
            var contentArray = content.split("##LLAMAR##").map(item => { 
                return {
                    text: item
                }
            });
            
            if (contentArray.length > 1) {
                contentArray.splice(1,0, { hasButton: true });
            }

            return Object.assign({}, rest, {content: contentArray});
        });

        this.setState({ data });
    }

    render() {
        const lastCard = this.state.data.length - 1;
        return (
            <ScrollView>
                {
                    this.state.data.map((item, i) => {
                        var titleStyle = { color: item.style.color, fontSize: 17 };
                        var cardStyle = { marginBottom: lastCard === i ? 17 : 0 };
                        return (
                            <Card key={i} title={item.title} titleStyle={titleStyle} containerStyle={cardStyle}> 
                            {
                                item.content.map((content, i) => {
                                    return content.text 
                                    ? (
                                        <Text key={i} style={styles.textCenter}>
                                            {content.text}
                                        </Text>
                                    ) :
                                    content.hasButton 
                                    && (
                                        <ButtonLlamar spacer={15} key={i} color={item.style.color} />
                                    )
                                })
                            }
                        </Card>
                    )})
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    textCenter: {
        textAlign: 'center'
    }
});
