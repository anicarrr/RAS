import React from "react";
import {
    StyleSheet,
    View,
    Image
} from 'react-native';
import randomString from 'src/utils/randomString';

const handleLayout = (evt, key) => {
    this[key].setNativeProps({
        height: evt.nativeEvent.layout.height,
        width: evt.nativeEvent.layout.height
    });
}

const FlexImage = (props) => {
    var _flex = props.flexContainer || 1;
    var key = randomString(6);
    return (
        <View style={[styles.imageContainer, { flex: _flex }]} onLayout={(evt) => handleLayout(evt, key)}>
            <Image
                ref = { element => this[key] = element }
                style={styles.defaultImageStyle}
                {...props}
                resizeMethod="scale"
                />
        </View>
    );
};

const styles = StyleSheet.create({
    imageContainer: {
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    defaultImageStyle: {
        height: 'auto', 
        width: 'auto'
    }
});

export default FlexImage;
