const FaqContent = [
    {
        title: '¿Qué debo hace en caso de urgencia y/o emergencia médica fuera de mi zona de residencia habitual?',
        content: `Para los casos en los que se presume RIESGO DE VIDA: SE DEBE PRIORIZAR LA ATENCION MEDICA y concurrir con URGENCIA a la guardia médica más próxima para que se establezcan cuidados médicos inmediatos que eviten males mayores en la salud del paciente. En caso de requerir asistencia médica por urgencia y/u emergencia médica y no suponga riesgo de vida, deberá comunicarse con el centro nacional de atención de 24hs de la Red Argentina de Salud para afiliados en tránsito.##LLAMAR##Importante: Recuerde que siempre que abone una prestación debe solicitar comprobantes y en lo posible un mínimo resumen clínico, a los efectos de facilitar reintegros, si asi lo correspondiera de acuerdo a la cobertura de su plan.`,
        style: {
            color: 'red'
        }
    },
    {
        title: '¿Cómo se tramita la derivación programada o la atención en Ia eventualidad de un viaje?',
        content: `En caso de requerir derivaciones programas es necesario que consulte previamente en su Sistema Médico, a los fines de coordinar todos los detalles de accesibilidad de atención en el lugar de destino. A tal efecto, se le entregará la correspondiente autorización de prestaciones y la información necesaria para el acceso a los servicios. En caso de no contar con la correspondiente orden de derivación, le recomendamos que previo a requerir la atención de prestadores de servicios de salud a los que no ha sido derivado, fuera de su zona de residencia habitual, se informe acerca de la modalidad de cobertura posible.`,
        style: {
            color: 'green'
        }
    },
    {
        title: '¿Qué debo hacer si extravié la credencial?',
        content: `Si extravió o sufrió el robo de la credencial, la orden de derivación para atención en tránsito y el DNI deberian ser suficiente documentación probatoria de la identidad. De no ser asi, le recomendamos comunicarse con:##LLAMAR##El Centro Nacional de Atención cuenta con los datos de todos los beneficiarios y tiene capacidad para gestionar su asistencia, aún sin credencial disponible. De regreso a su zona de residencia habitual deberá efectuar la denuncia de perdida y gestionar una nueva credencial.`,
        style: {
            color: 'green'
        }
    },
    {
        title: '¿Si utilizo servicios privados, podré solicitar reintegro a mi regreso?',
        content: `Recuerde que siempre que abone una prestación debe solicitar los correspondientes comprobantes de pago y en lo posible un mínimo resumen clínico, a los efectos de facilitar los reintegros, si asi le correspondiera de acuerdo a la cobertura de su plan.`,
        style: {
            color: 'green'
        }
    }
];

export default FaqContent;