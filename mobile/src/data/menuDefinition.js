import Colors from '../utils/colors';
import Services from '../utils/services';

const iconCommonStyles = {
  color: Colors.blueRas,
}

export const MenuItems = [
  {
    title: 'Carnet de Afiliados',
    icon: {
      name: 'id-card-o',
      type: 'font-awesome',
      ...iconCommonStyles
    },
    action: {
      type: 'modal',
      params: {
        contentToShow: 'dni',
        headerTitle: 'Carnet de Afiliados',
        screen: 'carnetAfiliados'
      }
    }
  },
  {
    title: 'Urgencias Emergencias',
    icon: {
      name: 'phone-in-talk',
      type: 'material-icons',
      ...iconCommonStyles
    },
    action: {
      type: 'navigation',
      params: {
        screenName: 'Urgencias'
      }
    }
  },
  {
    title: 'Entidades RAS',
    icon: {
      name: 'map-marker',
      type: 'font-awesome',
      ...iconCommonStyles
    },
    action: {
      type: 'navigation',
      params: {
        screenName: 'Entidades'
      }
    }
  },
  {
    title: 'Carnet Club de Beneficios RAS',
    icon: {
      name: 'credit-card',
      type: 'font-awesome',
      ...iconCommonStyles
    },
    action: {
      type: 'modal',
      params: {
        contentToShow: 'dni',
        headerTitle: 'Carnet de Beneficios',
        screen: 'carnetBeneficios'
      }
    }
  },
  {
    title: 'Club de Beneficios',
    icon: {
      name: 'search',
      type: 'font-awesome',
      ...iconCommonStyles
    },
    action: {
      type: 'url',
      params: {
        url: () => Services.getMenuUrls().buscadorDeBeneficios
      }
    }
  },
  {
    title: 'Preguntas Frecuentes',
    icon: {
      name: 'question',
      type: 'font-awesome',
      ...iconCommonStyles
    },
    action: {
      type: 'navigation',
      params: {
        screenName: 'Faq'
      }
    }
  },
  {
    title: 'Web',
    icon: {
      name: 'web',
      type: 'material-community',
      ...iconCommonStyles
    },
    action: {
      type: 'url',
      params: {
        url: () => Services.getMenuUrls().webPrincipal
      }
    }
  },
]