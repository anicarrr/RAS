export const EntidadesMapData = [
    {
        title: 'RAS Sede Central',
        phone: '(011) 4383-5511',
        address: 'Av. Belgrano 1235',
        city: 'Ciudad de Buenos Aires',
        coordinates: {
            latitude: -34.6131231,
            longitude: -58.38372400000003 
        }
    },
    {
        title: 'FEMEDICA',
        phone: '(011) 4370-1700',
        address: 'Hipolito Yrigoyen 1126',
        city: 'Ciudad de Buenos Aires',
        coordinates: {
            latitude: -34.6098445,
            longitude: -58.382364199999984 
        }
    },
    {
        title: 'CIMESAN SALUD',
        phone: '(0336) 4436738 y (0336) 4453972',
        address: 'Guardias Nacionales 17',
        city: 'SAN NICOLAS (Pcia. Bs. As)',
        coordinates: {
            latitude: -33.328151,
            longitude: -60.215243
        }
    }
];

export default EntidadesMapData;