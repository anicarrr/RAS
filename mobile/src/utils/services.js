import { NetInfo } from 'react-native';
import FaqContent from 'src/data/faqData'

class appData {
    constructor(
        carnetDeAfiliados = "https://www.rascomra.com.ar/2016/ws_carnet.php?dni=${dni}&android=1",
        carnetDeBeneficios = "https://www.rascomra.com.ar/2016/ws_carnet.php?dni=${dni}&beneficios=1",
        webPrincipal = 'http://www.rascomra.com.ar',
        buscadorDeBeneficios = 'http://www.rascomra.com.ar/2016/club-de-beneficios.html',
        entidades = [],
        faq = FaqContent,
        telefonos = { emergencia: '08003333169'}) 
        {
            this.carnetDeAfiliados = carnetDeAfiliados;
            this.carnetDeBeneficios = carnetDeBeneficios;
            this.webPrincipal = webPrincipal;
            this.buscadorDeBeneficios = buscadorDeBeneficios;
            this.entidades = entidades;
            this.telefonos = telefonos;
            this.faq = faq;
        }
    }
    
const data = new appData();
var hasDataBeenFetched = false;

const fetchData = (url) => {
    return fetch(url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            ContentType: 'application/json'
        },
    })
}

const objectToArray = (object) => {
    return Object.keys(object || {}).map(k => object[k]); 
}

const entidadesBuilder = (object) => {
    var entidades = objectToArray(object);
    var data = entidades.map(entidad => {
        var { latitude, longitude, ...otherProps } = entidad;
        return {
            ...otherProps,
            coordinates: {
                latitude: Number(latitude),
                longitude: Number(longitude)
            }
        }
    });

    data.entidades = data;

    return data;
}

const faqBuilder = (object) => {
    return objectToArray(object);
}

const getEntidadesFromDB = () => {
    return fetchData(`https://red-argentina-de-salud.firebaseio.com/data/entidades.json`).then(entidadesBuilder);
}

const getCarnetDeAfiliados = (dni) => {
    return data.carnetDeAfiliados.replace("${dni}", dni);
}

const getCarnetDeBeneficios = (dni) => {
    return data.carnetDeBeneficios.replace("${dni}", dni);
}

const getMenuUrls = () => {
    return { 
        webPrincipal: data.webPrincipal,
        buscadorDeBeneficios: data.buscadorDeBeneficios
    }
}

const getStoreUrls = () => {
    return {
        android: data.storeAndroid,
        ios: data.storeIos        
    }
}

const getEntidades = () => {
    return data.entidades.length ? new Promise(resolve => resolve(data.entidades)) : getEntidadesFromDB();
}

const getTelefonos = () => {
    return data.telefonos;
}

const getFaq = () => {
    return data.faq;
}

const initAppData = () => {
    return fetchData(`https://red-argentina-de-salud.firebaseio.com/data.json`)
        .then(response => {
        var _data = JSON.parse(response._bodyInit);     
        var entidades = entidadesBuilder(_data.entidades);
        var faq = faqBuilder(_data.faq);

        data.carnetDeAfiliados = _data.urls.carnetDeAfiliados;
        data.carnetDeBeneficios = _data.urls.carnetDeBeneficios;
        data.webPrincipal = _data.urls.webPrincipal;
        data.buscadorDeBeneficios = _data.urls.buscadorDeBeneficios;
        data.telefonos = _data.telefonos;
        data.entidades = entidades;
        data.faq = faq;
        data.storeAndroid = _data.urls.storeAndroid;
        data.storeIos = _data.urls.storeIos;

        hasDataBeenFetched = true;
    });
}

const Services = { 
    getEntidades, 
    getCarnetDeAfiliados, 
    getCarnetDeBeneficios,
    getMenuUrls,
    getTelefonos,
    getFaq,
    getStoreUrls,
    initAppData  
};

(() => {
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        (isConnected) => {
            if (isConnected && !hasDataBeenFetched) {
                initAppData();
            }
        }
    );
})();

export default Services;