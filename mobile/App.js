import React from 'react';
import { Platform } from 'react-native';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './src/components/HomeScreen';
import UrgenciasScreen from './src/components/UrgenciasScreen';
import FaqScreen from './src/components/FaqScreen';
import EntidadesScreen from './src/components/EntidadesScreen';
import CarnetScreen from './src/components/CarnetScreen';
import SplashScreen from 'react-native-splash-screen';
import Services from './src/utils/services';
import fetch from 'react-native-fetch-polyfill';

const Routes = StackNavigator({
  Home: { screen: HomeScreen },
  Urgencias: { screen: UrgenciasScreen},
  Faq: { screen: FaqScreen},
  Entidades: { screen: EntidadesScreen },
  Carnet: { screen: CarnetScreen }
});

export default class App extends React.Component {

  componentDidMount() {
    if (Platform.OS === 'android') {
      SplashScreen.show();
    }

    setTimeout(() => {
      fetch(Services.initAppData(), { timeout: 3 * 1000 })
        .then(() => {
          SplashScreen.hide();
        })
        .catch(() => {
          SplashScreen.hide();
        })
    },3500);
  }
  
  render() {
    return <Routes />;
  }
}