import React from 'react';
import { ValidatorComponent } from 'react-form-validator-core';
import TextField from 'material-ui/TextField';

class TextValidator extends ValidatorComponent {

    render() {
        const { errorMessages, validators, requiredError, validatorListener, ...rest } = this.props;
        const { isValid } = this.state;
        const message = isValid ? '' : this.getErrorMessage();
        return (
                <TextField
                    {...rest}
                    helperText={ message }
                    error={ !isValid }
                />
        );
    }
  
}

export default TextValidator;