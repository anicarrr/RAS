const database = window.firebase.database();

const getKeysAndMap = (data, fn) => (Object.keys(data.val() || {}).map(fn));
const objectToArray = data => getKeysAndMap(data, k => data.val()[k]);
const keyToProp = data => getKeysAndMap(data, k => ({ title: k, value: data.val()[k]}));

const get = (item, shouldTransform = true) => (
    database
        .ref(`/data/${item}`)
        .once('value')
        .then(s => shouldTransform ? objectToArray(s) : keyToProp(s))
);

const save = (item, name) => {
    var uri = `/data/${name}`;
    if (item.id) {
        var itemToUpdate = {};
        itemToUpdate[`/${item.id}`] = item.value || item;
        return database
            .ref(uri)
            .update(itemToUpdate);
    } else {
        var newItem = database.ref(uri).push();
        Object.assign(item, { id: newItem.key });
        return newItem.set(item);
    }
}

const remove = (id, name) => {
    return database
        .ref(`/data/${name}/${id}`)
        .remove();
}

const login = (user, password) => {
    return window.firebase.auth().signInWithEmailAndPassword(user, password)
    .then(data => data)
    .catch(error => {
        var response = {
            error: true,
            message: ''
        };

        const invalidCredentials = error.code === 'auth/wrong-password';

        response.message = invalidCredentials
            ? 'Usuario y/o contraseña invalido/s'
            : 'No se pudo conectar con el servidor, intente mas tarde';

        return response;
    });
};

const getEntidades = () => get('entidades');
const saveEntidad = (item) => save(item, 'entidades');
const removeEntidad = (id) => remove(id, 'entidades');

const getTelefonos = () => get('telefonos', false);
const saveTelefonos = (item) => save(item, 'telefonos');

const getUrls = () => get('urls', false);
const saveUrls = (item) => save(item, 'urls');

const getFAQs = () => get('faq');
const saveFAQ = (item) => save(item, 'faq');
const removeFAQ = (id) => remove(id, 'faq');

const Services = { login,
                   getEntidades,
                   saveEntidad,
                   removeEntidad,
                   getTelefonos,
                   saveTelefonos,
                   getFAQs,
                   saveFAQ,
                   removeFAQ,
                   getUrls,
                   saveUrls
                };

export default Services;