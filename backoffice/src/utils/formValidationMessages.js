export const validationMessages = {
    required: 'Este campo es requerido',
    isCoordinate: 'Este campo debe ser una coordenada válida (ej: -45.323565650)',
    maxStringLength: 'El limite de caracteres es de 50',
    isNumber: 'El valor ingresado debe ser un número',
    isValidUrl: 'El valor ingresado no es una URL válida'
};