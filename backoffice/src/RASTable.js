import React from 'react';
import PropTypes from 'prop-types';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import DeleteIcon from 'material-ui-icons/Delete';
import EditIcon from 'material-ui-icons/Edit';
import Button from 'material-ui/Button/Button';
import AddButton from 'material-ui-icons/AddCircleOutline';
import TextField from 'material-ui/TextField';
import TableToolBar from './TableToolbar';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';

import './App.css';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: 0
    },
    table: {
        minWidth: 800
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    centerSpinner: {
        position: 'relative',
        left: '500%'
    },
    pointer: {
        cursor: 'pointer'
    },
    spacerLeft: {
        marginLeft: '23px',
        paddingBottom: '15px'
    },
    leftIcon: {
        marginRight: theme.spacing.unit
    },
    buttonAdd: {
        marginLeft: 30,
        minWidth: '125px'
    }
});

class EnhancedTable extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            item: {},
            data: [],
            page: 0,
            rowsPerPage: 10,
            openModal: false,
            loading: false,
            searchValue: ''
        };
    }

    modalType = '';
    requestOpenModal = false;
    dataRaw = [];

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    componentDidMount = () => {
        this.handleRefresh();
    }

    handleRefresh = () => {
        this.setState({ loading: true }, () => {
            this.props.fn().then(data => {
                this.setState({ data, loading: false, searchValue: '' }, () => {
                    Object.assign(this.dataRaw, this.state.data);
                });
            });
        });
    }

    handleDelete = (event, element) => {
        this.setState({
            item: element
        });
        this.requestOpenModal = true;
        this.modalType = 'delete';
    }

    handleEdit = (event, element) => {
        this.setState({
            item: element
        });
        this.requestOpenModal = true;
        this.modalType = 'update';
    }

    handleAdd = () => {
        this.setState({
            item: {}
        });
        this.requestOpenModal = true;
        this.modalType = 'create';
    }

    handleClearModalType = () => {
        this.modalType = '';
        this.requestOpenModal = false;
    }

    handleSearch = (event) => {
        var searchValue = event.target.value.toLowerCase();
        var filterData = this.dataRaw.filter(item => item.title.toLowerCase().includes(searchValue));
        this.setState({ data: filterData, searchValue });
    }

    render() {
        const { classes, includeAddButton, includeSearchBy, includeDelete, title, columnData, content, ModalComp } = this.props;
        const { data, item, rowsPerPage, page, loading, searchValue } = this.state;

        const hasEdit = !!columnData.find(x => x.id === 'Edit');

        if (!hasEdit) {
            columnData.push({ id: 'Edit', label: '' });
        }

        if (includeDelete) {
            const hasDelete = !!columnData.find(x => x.id === 'Delete');
            if (!hasDelete) {
                columnData.push({ id: 'Delete', label: '' });
            }
        }

        return (
            <Paper className={classes.root}>
                <TableToolBar title={title}>
                {
                includeAddButton &&
                    <Button className={classes.buttonAdd} raised color="primary" onClick={() => this.handleAdd()}>
                        <AddButton className={classes.leftIcon} />
                        Agregar
                    </Button>
                }
                </TableToolBar>
                {
                    includeSearchBy &&
                    <div className={classes.spacerLeft}>
                        <TextField
                            onChange={this.handleSearch}
                            label="Filtrar por Título"
                            name="Filtrar por Título"
                            value={searchValue}
                        />
                    </div>
                }

                <div className={classes.tableWrapper}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                {columnData.map((column, i) => {
                                    return (
                                        <TableCell
                                            key={i}
                                            padding={'default'}
                                        >
                                            {column.label}
                                        </TableCell>
                                    );
                                }, this)}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                loading
                                    ? (
                                        <TableRow>
                                            <div className="center-spinner-table">
                                                <CircularProgress size={50} />
                                            </div>
                                        </TableRow>
                                    )
                                    :
                                    data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n, i) => {
                                        return (
                                            <TableRow
                                                hover
                                                tabIndex={-1}
                                                key={i}
                                            >
                                                {
                                                    content(n)
                                                }
                                                <TableCell className={classes.pointer} onClick={event => this.handleEdit(event, n)}>
                                                    <EditIcon />
                                                </TableCell>
                                                {
                                                    includeDelete &&
                                                    <TableCell className={classes.pointer} onClick={event => this.handleDelete(event, n)}>
                                                        <DeleteIcon />
                                                    </TableCell>
                                                }
                                            </TableRow>
                                        );
                                    })
                            }
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    count={data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page'
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page'
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
                {
                    this.requestOpenModal &&
                    <ModalComp
                        open={this.requestOpenModal}
                        type={this.modalType}
                        item={item}
                        onRefresh={this.handleRefresh}
                        onClearModalType={this.handleClearModalType}
                    />
                }
            </Paper>
        );
    }
}

EnhancedTable.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EnhancedTable);