import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MenuList, MenuItem } from 'material-ui/Menu';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import { ListItemIcon, ListItemText } from 'material-ui/List';
import LocationIcon from 'material-ui-icons/LocationOn';
import PhoneIcon from 'material-ui-icons/PhoneInTalk';
import QuestionIcon from 'material-ui-icons/QuestionAnswer';
import LinkIcon from 'material-ui-icons/Link';
import { Link } from 'react-router-dom';

const palleteNumber = 500;

const styles = theme => ({
    menuItem: {
        '&:focus': {
            background: theme.palette.primary[palleteNumber],
            '& $text, & $icon': {
                color: theme.palette.common.white
            }
        }
    },
    text: {},
    icon: {},
    link: {
        textDecoration: 'none'
    }
});

class Menu extends Component {

    render(){
        const { classes } = this.props;

        return (
            <Paper>
                <MenuList>
                    <Link className={classes.link} to="/entidades">
                        <MenuItem className={classes.menuItem}>
                            <ListItemIcon className={classes.icon}>
                                <LocationIcon />
                            </ListItemIcon>
                            <ListItemText classes={{ text: classes.text }} inset primary="Entidades" />
                        </MenuItem>
                    </Link>
                    <Link className={classes.link} to="/telefono">
                        <MenuItem className={classes.menuItem}>
                            <ListItemIcon className={classes.icon}>
                                <PhoneIcon />
                            </ListItemIcon>
                            <ListItemText classes={{ text: classes.text }} inset primary="Teléfono" />
                        </MenuItem>
                    </Link>
                    <Link className={classes.link} to="/preguntasFrecuentes">
                        <MenuItem className={classes.menuItem}>
                            <ListItemIcon className={classes.icon}>
                                <QuestionIcon />
                            </ListItemIcon>
                            <ListItemText classes={{ text: classes.text }} inset primary="Preguntas Fr." />
                        </MenuItem>
                    </Link>
                    <Link className={classes.link} to="/urls">
                        <MenuItem className={classes.menuItem}>
                            <ListItemIcon className={classes.icon}>
                                <LinkIcon />
                            </ListItemIcon>
                            <ListItemText classes={{ text: classes.text }} inset primary="Urls" />
                        </MenuItem>
                    </Link>
                </MenuList>
            </Paper>
        );
    }
}

Menu.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Menu);
