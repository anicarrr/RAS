import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Card, { CardActions, CardContent, CardHeader } from 'material-ui/Card';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Services from './utils/services';
import Notification from './utils/notification';
import { Redirect } from 'react-router-dom';
import './App.css';
import { CircularProgress } from 'material-ui/Progress';

const styles = theme => ({
    container: {
        height: 240
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'column'
    },
    actionContent: {
        justifyContent: 'center',
        padding: '25px 4px 14px 1px'
    },
    card: {
        minWidth: 275,
        padding: 35
    },
    formControl: {
        margin: theme.spacing.unit
    },
    button: {
        margin: theme.spacing.unit,
        width: '100%'
    },
    textCenter: {
        textAlign: 'center'
    }
});

class Login extends Component {
    state = {
        user: '',
        password: '',
        redirectToDashboard: false,
        loading: false
    };

    handleUserInputChange = event => {
        this.setState({ user: event.target.value });
    };

    handlePasswordInputChange = event => {
        this.setState({ password: event.target.value });
    };

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.handleOnLogin();
        }
    }

    handleOnLogin = () => {
        const { user, password } = this.state;
        this.setState({loading: true}, () => {
            Services.login(user, password)
                .then(data => {
                    if (data.error) {
                        Notification.error(data.message);
                        this.setState({loading: false});
                        return;
                    }
                    this.setState({
                        redirectToDashboard: true,
                        loading: false
                    });
                });
        });
    }

    render() {
        const { classes } = this.props;
        const { redirectToDashboard, loading } = this.state;

        if (redirectToDashboard) {
            return (
                <Redirect to={{
                    pathname: '/dashboard',
                    state: { isAuthenticated: true }
                }} />
            );
        }

        return (
            <div className="center">
                <Card className={classes.card}>
                    <CardHeader title='RAS Dashboard' className={classes.textCenter} />
                    <CardContent className={classes.cardContent}>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="user-input">Usuario</InputLabel>
                            <Input id="user-input"
                                    value={this.state.user}
                                    onChange={this.handleUserInputChange}
                                    onKeyPress={this.handleKeyPress} />
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="password-input">Contraseña</InputLabel>
                            <Input id="password-input"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.handlePasswordInputChange}
                                    onKeyPress={this.handleKeyPress} />
                        </FormControl>
                    </CardContent>
                    <CardActions className={classes.actionContent}>
                        {
                            loading
                            ? <CircularProgress size={50} />
                            : <Button raised color="primary" onClick={this.handleOnLogin} className={classes.button}>
                                Ingresar
                              </Button>
                        }
                    </CardActions>
                </Card>
            </div>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Login);
