import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import Menu from './Menu';
import Entidades from './views/entidades/Entidades';
import Telefonos from './views/telefonos/Telefonos';
import Urls from './views/urls/Urls';
import Faq from './views/faqs/Faq';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import CloseSession from './CloseSession';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: 16,
        marginLeft: 10,
        marginRight: 10,
        textAlign: 'center',
        color: theme.palette.text.secondary
    }
});

class Dashboard extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Router basename="/dashboard">
                <div className={classes.root}>
                    <CloseSession />
                    <Grid container spacing={0}>
                        <Grid item xs={2}>
                            <Menu />
                        </Grid>
                        <Grid item xs={10}>
                            <div>
                                <Switch>
                                    <Route path="/entidades" component={Entidades} />
                                    <Route path="/telefono" component={Telefonos} />
                                    <Route path="/preguntasFrecuentes" component={Faq} />
                                    <Route path="/urls" component={Urls} />
                                    <Redirect from="" to="/entidades" />
                                </Switch>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </Router>
        );
    }
}

Dashboard.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);
