import React from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Services from './../../utils/services';
import TextValidator from './../../utils/TextValidator';
import { ValidatorForm } from 'react-form-validator-core';
import { validationMessages } from './../../utils/formValidationMessages';
import RASModal from '../../RASModal';

function getModalStyle() {
    return {
        position: 'absolute',
        minWidth: 600,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #e5e5e5',
        backgroundColor: '#fff',
        boxShadow: '0 5px 15px rgba(0, 0, 0, .5)',
        padding: 32
    };
}

const styles = theme => ({
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: 10,
        width: '100%'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 15
    },
    spaceRight: {
        marginRight: 10
    },
    textCenter: {
        textAlign: 'center'
    }
});

ValidatorForm.addValidationRule('isValidUrl', (value) => {
  return /(http[s]?:\/\/)?[^\s(["<,>]*\.[^\s[",><]*/.test(value);
});

class FormModal extends React.Component {
    state = {
        url: {
            id: '',
            title: '',
            value: 0
        }
    }

    handleOnChange = (prop, value) => {
        this.setState(prevState => ({
            ...prevState,
            url: {
                ...prevState.url,
                [prop]: value
            }
        }));
    }

    componentWillMount = () => {
        if (!this.props.item.value) return;
        const { title, value } = this.props.item;
        this.setState(prevState => ({
            ...prevState,
            url: { id: title, title, value }
        }));
    }

    render() {
        const { classes, onCloseModal, onCRUD, titleModal, type, disableButton } = this.props;
        const { value } = this.state.url;
        const { required, isValidUrl } = validationMessages;
        return (
            <div style={getModalStyle()}>
                <div>
                    <Typography className={classes.textCenter} type="title">
                        {titleModal}
                    </Typography>
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={() => onCRUD(this.state.url, type)}
                >
                    <div className={classes.container}>
                        <TextValidator
                            label="Url"
                            name="url"
                            value={value}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('value', event.target.value)}
                            validators={['required', 'isValidUrl']}
                            errorMessages={[required, isValidUrl]}
                        />
                    </div>
                    <div className={classes.buttonContainer}>
                        <Button
                            disabled={disableButton}
                            raised
                            color="accent"
                            className={styles.button}
                            onClick={() => onCloseModal()}
                        >
                            Cancelar
                        </Button>
                        <Button
                            type="submit"
                            disabled={disableButton}
                            style={styles.marginRight}
                            raised color="primary"
                            className={classes.button}
                        >
                            Guardar
                        </Button>
                    </div>
                </ValidatorForm>
            </div>
        );
    }
}

class UrlsModal extends React.Component {
    render() {
        return (
            <RASModal
                FormModal={FormModal}
                saveFn={Services.saveUrls}
                title="Urls"
                {...this.props}
            />
        );
    }
}

export default withStyles(styles)(UrlsModal);