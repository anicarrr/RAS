import React, { Component } from 'react';
import Services from './../../utils/services';
import RasTable from './../../RASTable';
import UrlsModal from './UrlsModal';
import { TableCell } from 'material-ui/Table';

class Urls extends Component {
    columnData = [
        { id: 'Titulo', label: 'Titulo' },
        { id: 'Valor', label: 'Valor' }
    ];

    content = n => [
        <TableCell>{n.title}</TableCell>,
        <TableCell>{n.value}</TableCell>
    ];

    render() {
        return (
            <RasTable
                title={'Urls'}
                fn={Services.getUrls}
                columnData={this.columnData}
                content={this.content}
                ModalComp={UrlsModal}
            />
        );
    }
}

export default Urls;