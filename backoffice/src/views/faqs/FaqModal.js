import React from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Services from './../../utils/services';
import TextValidator from './../../utils/TextValidator';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import { ValidatorForm } from 'react-form-validator-core';
import { InputLabel } from 'material-ui/Input';
import { validationMessages } from './../../utils/formValidationMessages';
import { FormControl } from 'material-ui/Form';
import RASModal from '../../RASModal';

function getModalStyle() {
    return {
        position: 'absolute',
        maxWidth: 600,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #e5e5e5',
        backgroundColor: '#fff',
        boxShadow: '0 5px 15px rgba(0, 0, 0, .5)',
        padding: 32
    };
}

const styles = theme => ({
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: 10,
        width: '100%'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 15
    },
    spaceRight: {
        marginRight: 10
    },
    textCenter: {
        textAlign: 'center'
    }
});

class FormModal extends React.Component {
    state = {
        faq: {
            id: 0,
            title: '',
            content: '',
            style: {
                color: 'red'
            }
        }
    }

    handleOnChange = (prop, value) => {
        if (prop.includes(".")) {
            const values = prop.split(".");
            this.setState(prevState => ({
                ...prevState,
                faq: {
                    ...prevState.faq,
                    [values[0]]: {
                        [values[1]]: value
                    }
                }
            }));
        } else {
            this.setState(prevState => ({
                ...prevState,
                faq: {
                    ...prevState.faq,
                    [prop]: value
                }
            }));
        }
    }

    componentWillMount = () => {
        if (!this.props.item.id) return;
        const { id, title, content, style } = this.props.item;
        this.setState(prevState => ({
            ...prevState,
            faq: { id, title, content, style }
        }));
    }

    render() {
        const { classes, onCloseModal, onCRUD, titleModal, type, disableButton } = this.props;
        const { title, content, style } = this.state.faq;
        const { required } = validationMessages;
        return (
            <div style={getModalStyle()}>
                <div>
                    <Typography className={classes.textCenter} type="title">
                        {titleModal}
                    </Typography>
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={() => onCRUD(this.state.faq, type)}
                >
                    <div className={classes.container}>
                        <FormControl style={{minWidth: 120, marginLeft: '5px'}}>
                            <InputLabel htmlFor="titleColor-simple">Color de título</InputLabel>
                            <Select
                                value={style.color}
                                onChange={event => this.handleOnChange('style.color', event.target.value)}
                                inputProps={{
                                    name: 'titleColor',
                                    id: 'titleColor-simple',
                                }}
                            >
                                <MenuItem value={'green'}>Verde</MenuItem>
                                <MenuItem value={'red'}>Rojo</MenuItem>
                            </Select>
                        </FormControl>
                        <TextValidator
                            label="Título"
                            name="titulo"
                            value={title}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('title', event.target.value)}
                            validators={['required']}
                            errorMessages={[required]}
                            multiline={true}
                        />
                        <TextValidator
                            label="Contenido"
                            name="contenido"
                            value={content}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('content', event.target.value)}
                            validators={['required']}
                            errorMessages={[required]}
                            multiline={true}
                        />
                    </div>
                    <div className={classes.buttonContainer}>
                        <Button
                            disabled={disableButton}
                            raised
                            color="accent"
                            className={styles.button}
                            onClick={() => onCloseModal()}
                        >
                            Cancelar
                        </Button>
                        <Button
                            type="submit"
                            disabled={disableButton}
                            style={styles.marginRight}
                            raised color="primary"
                            className={classes.button}
                        >
                            Guardar
                        </Button>
                    </div>
                </ValidatorForm>
            </div>
        );
    }
}

class FaqModal extends React.Component {
    render() {
        const title = this.props.item.title;
        const deleteTitle = title ? `${title.substring(0,50)}...` : '';
        return (
            <RASModal
                FormModal={FormModal}
                saveFn={Services.saveFAQ}
                deleteFn={Services.removeFAQ}
                includeDelete={true}
                deleteTitle={deleteTitle}
                title="Pregunta Frecuente"
                {...this.props}
            />
        );
    }
}

export default withStyles(styles)(FaqModal);