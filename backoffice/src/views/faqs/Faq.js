import React, { Component } from 'react';
import Services from './../../utils/services';
import RasTable from './../../RASTable';
import { TableCell } from 'material-ui/Table';
import FaqModal from './FaqModal';

class Urls extends Component {
    columnData = [
        { id: 'Titulo', label: 'Titulo' },
        { id: 'Contenido', label: 'Contenido' },
        { id: 'Estilo', label: 'Color del Título' }
    ];

    content = n => [
        <TableCell>{n.title}</TableCell>,
        <TableCell>{`${n.content.substring(0,70)}...`}</TableCell>,
        <TableCell>{n.style.color}</TableCell>
    ];

    render() {
        return (
            <RasTable
                title={'Preguntas Frecuentes'}
                fn={Services.getFAQs}
                columnData={this.columnData}
                content={this.content}
                includeAddButton={true}
                includeDelete={true}
                includeSearchBy={true}
                ModalComp={FaqModal}
            />
        );
    }
}

export default Urls;