import React from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Services from './../../utils/services';
import TextValidator from './../../utils/TextValidator';
import { ValidatorForm } from 'react-form-validator-core';
import { validationMessages } from './../../utils/formValidationMessages';
import RASModal from '../../RASModal';

function getModalStyle() {
    return {
        position: 'absolute',
        maxWidth: 600,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #e5e5e5',
        backgroundColor: '#fff',
        boxShadow: '0 5px 15px rgba(0, 0, 0, .5)',
        padding: 32
    };
}

const styles = theme => ({
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: 10,
        width: '100%'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 15
    },
    spaceRight: {
        marginRight: 10
    },
    textCenter: {
        textAlign: 'center'
    }
});

ValidatorForm.addValidationRule('isCoordinate', (value) => {
    return /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/.test(value);
});

class FormModal extends React.Component {
    state = {
        entidad: {
            id: 0,
            title: '',
            phone: '',
            address: '',
            city: '',
            latitude: '',
            longitude: ''
        }
    }

    handleOnChange = (prop, value) => {
        this.setState(prevState => ({
            ...prevState,
            entidad: {
                ...prevState.entidad,
                [prop]: value
            }
        }));
    }

    componentWillMount = () => {
        if (!this.props.item.id) return;
        const { id, title, phone, address, city, latitude, longitude } = this.props.item;
        this.setState(prevState => ({
            ...prevState,
            entidad: { id, title, phone, address, city, latitude, longitude }
        }));
    }

    render(){
        const { classes, onCloseModal, onCRUD, titleModal, type, disableButton } = this.props;
        const { title, phone, address, city, latitude, longitude } = this.state.entidad;
        const { required, isCoordinate, maxStringLength } = validationMessages;
        return (
            <div style={getModalStyle()}>
                <div>
                    <Typography className={classes.textCenter} type="title">
                        {titleModal}
                    </Typography>
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={() => onCRUD(this.state.entidad, type)}
                >
                    <div className={classes.container}>
                        <TextValidator
                            label="Título"
                            name="titulo"
                            value={title}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('title', event.target.value) }
                            validators={['required', 'maxStringLength:50']}
                            errorMessages={[required, maxStringLength]}
                        />
                        <TextValidator
                            label="Teléfono"
                            name="telefono"
                            value={phone}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('phone', event.target.value)}
                            validators={['required', 'maxStringLength:50']}
                            errorMessages={[required, maxStringLength]}
                        />
                        <TextValidator
                            label="Dirección"
                            name="address"
                            value={address}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('address', event.target.value)}
                            validators={['required', 'maxStringLength:50']}
                            errorMessages={[required, maxStringLength]}
                        />
                        <TextValidator
                            label="Ciudad"
                            name="city"
                            value={city}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('city', event.target.value)}
                            validators={['required', 'maxStringLength:50']}
                            errorMessages={[required, maxStringLength]}
                        />
                        <TextValidator
                            label="Latitud"
                            name="latitude"
                            value={latitude}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('latitude', event.target.value)}
                            validators={['required', 'maxStringLength:50', 'isCoordinate']}
                            errorMessages={[required, maxStringLength, isCoordinate]}
                        />
                        <TextValidator
                            label="Longitud"
                            name="longitude"
                            value={longitude}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('longitude', event.target.value)}
                            validators={['required', 'maxStringLength:50', 'isCoordinate']}
                            errorMessages={[required, maxStringLength, isCoordinate]}
                        />
                    </div>
                    <div className={classes.buttonContainer}>
                        <Button
                            disabled={disableButton}
                            raised
                            color="accent"
                            className={styles.button}
                            onClick={() => onCloseModal()}
                        >
                            Cancelar
                        </Button>
                        <Button
                            type="submit"
                            disabled={disableButton}
                            style={styles.marginRight}
                            raised color="primary"
                            className={classes.button}
                        >
                            Guardar
                        </Button>
                    </div>
                </ValidatorForm>
            </div>
        );
    }
}

class EntidadModal extends React.Component {
    render() {
        const title = this.props.item.title;
        return (
            <RASModal
                FormModal={FormModal}
                saveFn={Services.saveEntidad}
                deleteFn={Services.removeEntidad}
                includeDelete={true}
                deleteTitle={title}
                title="Entidad"
                {...this.props}
            />
        );
    }
}

export default withStyles(styles)(EntidadModal);