import React, { Component } from 'react';
import Services from './../../utils/services';
import RasTable from './../../RASTable';
import { TableCell } from 'material-ui/Table';
import EntidadModal from './EntidadModal';

class Urls extends Component {
    columnData = [
        { id: 'Titulo', label: 'Titulo' },
        { id: 'Telefono', label: 'Teléfono' },
        { id: 'Dirección', label: 'Dirección' },
        { id: 'Ciudad', label: 'Ciudad' },
        { id: 'Latitud', label: 'Latitud' },
        { id: 'Longitud', label: 'Longitud' }
    ];

    content = n => [
        <TableCell>{n.title}</TableCell>,
        <TableCell>{n.phone}</TableCell>,
        <TableCell>{n.address}</TableCell>,
        <TableCell>{n.city}</TableCell>,
        <TableCell>{n.latitude}</TableCell>,
        <TableCell>{n.longitude}</TableCell>
    ];

    render() {
        return (
            <RasTable
                title={'Entidades'}
                fn={Services.getEntidades}
                columnData={this.columnData}
                content={this.content}
                includeAddButton={true}
                includeDelete={true}
                includeSearchBy={true}
                ModalComp={EntidadModal}
            />
        );
    }
}

export default Urls;