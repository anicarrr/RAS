import React, { Component } from 'react';
import Services from './../../utils/services';
import RasTable from './../../RASTable';
import TelefonosModal from './TelefonosModal';
import { TableCell } from 'material-ui/Table';

class Telefonos extends Component {
    columnData = [
        { id: 'Titulo', label: 'Titulo' },
        { id: 'Número', label: 'Número' }
    ];

    content = n => [
        <TableCell>{n.title}</TableCell>,
        <TableCell>{n.value}</TableCell>
    ];

    render() {
        return (
            <RasTable
                title={'Telefonos'}
                fn={Services.getTelefonos}
                columnData={this.columnData}
                content={this.content}
                ModalComp={TelefonosModal}
            />
        );
    }
}

export default Telefonos;