import React from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Services from './../../utils/services';
import TextValidator from './../../utils/TextValidator';
import { ValidatorForm } from 'react-form-validator-core';
import { validationMessages } from './../../utils/formValidationMessages';
import RASModal from '../../RASModal';

function getModalStyle() {
    return {
        position: 'absolute',
        maxWidth: 600,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #e5e5e5',
        backgroundColor: '#fff',
        boxShadow: '0 5px 15px rgba(0, 0, 0, .5)',
        padding: 32
    };
}

const styles = theme => ({
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: 10,
        width: '100%'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 15
    },
    spaceRight: {
        marginRight: 10
    },
    textCenter: {
        textAlign: 'center'
    }
});

class FormModal extends React.Component {
    state = {
        telefono: {
            id: '',
            title: '',
            value: 0
        }
    }

    handleOnChange = (prop, value) => {
        this.setState(prevState => ({
            ...prevState,
            telefono: {
                ...prevState.telefono,
                [prop]: value
            }
        }));
    }

    componentWillMount = () => {
        if (!this.props.item.value) return;
        const { title, value } = this.props.item;
        this.setState(prevState => ({
            ...prevState,
            telefono: { id: title, title, value }
        }));
    }

    render() {
        const { classes, onCloseModal, onCRUD, titleModal, type, disableButton } = this.props;
        const { value } = this.state.telefono;
        const { required, isNumber, maxStringLength } = validationMessages;
        return (
            <div style={getModalStyle()}>
                <div>
                    <Typography className={classes.textCenter} type="title">
                        {titleModal}
                    </Typography>
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={() => onCRUD(this.state.telefono, type)}
                >
                    <div className={classes.container}>
                        <TextValidator
                            label="Número Emergencia"
                            name="número"
                            value={value}
                            className={classes.textField}
                            onChange={event => this.handleOnChange('value', event.target.value)}
                            validators={['required', 'isNumber', 'maxStringLength:30']}
                            errorMessages={[required, isNumber, maxStringLength]}
                            multiline={true}
                        />
                    </div>
                    <div className={classes.buttonContainer}>
                        <Button
                            disabled={disableButton}
                            raised
                            color="accent"
                            className={styles.button}
                            onClick={() => onCloseModal()}
                        >
                            Cancelar
                        </Button>
                        <Button
                            type="submit"
                            disabled={disableButton}
                            style={styles.marginRight}
                            raised color="primary"
                            className={classes.button}
                        >
                            Guardar
                        </Button>
                    </div>
                </ValidatorForm>
            </div>
        );
    }
}

class TelefonosModal extends React.Component {
    render() {
        return (
            <RASModal
                FormModal={FormModal}
                saveFn={Services.saveTelefonos}
                title="Teléfono"
                {...this.props}
            />
        );
    }
}

export default withStyles(styles)(TelefonosModal);