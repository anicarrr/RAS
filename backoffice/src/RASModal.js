import React from 'react';
import Typography from 'material-ui/Typography';
import Modal from 'material-ui/Modal';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Notification from './utils/notification';

function getModalStyle() {
    return {
        position: 'absolute',
        maxWidth: 600,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #e5e5e5',
        backgroundColor: '#fff',
        boxShadow: '0 5px 15px rgba(0, 0, 0, .5)',
        padding: 32
    };
}

const styles = theme => ({
    button: {
        margin: theme.spacing.unit
    },
    input: {
        display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: 10,
        width: '100%'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 15
    },
    spaceRight: {
        marginRight: 10
    },
    textCenter: {
        textAlign: 'center'
    }
});

const DeleteModal = ({ title, classes, onCloseModal, id, type, onCRUD, disableButton }) => {
    return (
        <div style={getModalStyle()}>
            <Typography type="title" id="modal-title">
                ¿Está seguro que desea borrar "{title}"?
            </Typography>
            <div className={classes.buttonContainer}>
                <Button
                    raised
                    color="accent"
                    disabled={disableButton}
                    className={styles.button}
                    onClick={() => onCloseModal()}
                >
                    No
                </Button>
                <Button
                    style={styles.marginRight}
                    raised
                    color="primary"
                    disabled={disableButton}
                    className={classes.button}
                    onClick={() => onCRUD(id, type)}
                >
                    Si
                </Button>
            </div>
        </div>
    );
};

class RASModal extends React.Component {
    state = {
        open: false,
        disableButton: false,
        type: ''
    };

    CrudAction = {
        create: item => this.handleOnCreate(item),
        update: item => this.handleOnUpdate(item),
        delete: id => this.handleOnDelete(id)
    }

    handleClose = () => {
        this.setState({
            open: false,
            type: ''
        });
        this.props.onClearModalType();
    };

    handleOnCreate = (item) => {
        if (!item) return;
        return this.props.saveFn(item)
        .then((e) => this.onSuccess('Item creado correctamente', e))
        .catch(() => this.onError('Hubo un error durante la creación, intente más tarde'));
    }

    handleOnUpdate = (item) => {
        if (!item) return;
        return this.props.saveFn(item)
        .then(this.onSuccess('Item actualizado correctamente'))
        .catch(this.onError('Hubo un error durante la actualización, intente más tarde'));
    }

    handleOnDelete = (id) => {
        if (!id) return;
        return this.props.deleteFn(id)
        .then(this.onSuccess('Item borrado correctamente'))
        .catch(this.onError('Hubo un error durante ek borrado, intente más tarde'));
    }

    handleOnCRUD = (item, type) => {
        this.setState({ disableButton: true });
        this.CrudAction[type](item)
        .then(() => {
            if (this.mounted) {
                this.setState({ disableButton: false });
            }
        });
    }

    onSuccess = (message, e) => {
        Notification.success(message);
        this.handleClose();
        this.props.onRefresh();
    }

    onError = message => error => {
        Notification.error(message);
        this.props.onRefresh();
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({
            open: nextProps.open,
            type: nextProps.type
        });
    }

    componentWillMount = () => {
        this.setState({
            open: this.props.open,
            type: this.props.type
        });
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const { item, classes, FormModal, includeDelete, deleteTitle, title } = this.props;
        const { disableButton, type, open } = this.state;
        return (
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={open}
                    onClose={this.handleClose}
                >
                    <div>
                    {
                        type === 'delete' && includeDelete
                        ? <DeleteModal
                            title={deleteTitle}
                            id={item.id}
                            type={type}
                            classes={classes}
                            onCloseModal={this.handleClose}
                            onCRUD={this.handleOnCRUD}
                            disableButton={disableButton}
                        />
                        :
                        type === 'update'
                        ? <FormModal
                            item={item}
                            classes={classes}
                            titleModal={`Editar ${title}`}
                            onCloseModal={this.handleClose}
                            onCRUD={this.handleOnCRUD}
                            type={type}
                            disableButton={disableButton}
                        />
                        :
                        type === 'create'
                        && <FormModal
                            item={{}}
                            titleModal={`Crear ${title}`}
                            classes={classes}
                            onCloseModal={this.handleClose}
                            onCRUD={this.handleOnCRUD}
                            type={type}
                            disableButton={disableButton}
                        />
                    }
                    </div>
                </Modal>
        );
    }
}

export default withStyles(styles)(RASModal);