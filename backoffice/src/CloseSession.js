import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import history from './utils/history';

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'flex-end',
        margin: '10px 25px 10px 0px',
        cursor: 'pointer',
        color: '#3f51b5'
    }
});

class CloseSession extends Component {
    handleCloseSesionClick = () => {
        window.firebase.auth().signOut()
        .then(() => {
            history.push('/');
        })
        .catch(() => {
            Notification.error('Hubo un problema intentando cerrar sesión');
        });
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.container} onClick={this.handleCloseSesionClick}>cerrar sesión</div>
        );
    }
}

CloseSession.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CloseSession);
