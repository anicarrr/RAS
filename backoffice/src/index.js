import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './Login';
import registerServiceWorker from './registerServiceWorker';
import Dashboard from './Dashboard';
import {
    Router,
    Route,
    Redirect,
} from 'react-router-dom';
import history from './utils/history';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        (props.location.state && props.location.state.isAuthenticated) 
            ? <Component {...props} />
            : <Redirect to="/" />
        )}
    />
);

const AppRouter = () => (
    <Router history={history}>
        <div>
            <Route exact path="/" component={Login} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
        </div>
    </Router>
);

ReactDOM.render(<AppRouter />, document.getElementById('root'));
registerServiceWorker();
